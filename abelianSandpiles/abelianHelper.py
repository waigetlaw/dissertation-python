import numpy as np

MAX_PILE = 3


def stabilize(a):
    """
    this stabilize function does not append extra space - sand is lost when going 'out of bounds'
    """

    arr = a.copy()
    x_limit, y_limit = arr.shape

    while (np.any(arr > MAX_PILE)):
        unstable_x, unstable_y = np.where(arr > MAX_PILE)

        # spill sand to other adjacent cells
        for x, y in zip(unstable_x, unstable_y):
            # amount of sand to send to each neighbour cell
            sand = arr[x, y]//4
            arr[x, y] = arr[x, y] % 4

            # up
            if (y + 1 < y_limit):
                arr[x, y+1] += sand

            # right
            if (x + 1 < x_limit):
                arr[x + 1, y] += sand

            # down
            if (y - 1 >= 0):
                arr[x, y - 1] += sand

            # left
            if (x - 1 >= 0):
                arr[x - 1, y] += sand
    return arr


def stabilizev2(a):
    """
    this stabilize function does not append extra space - sand is lost when going 'out of bounds'
    """

    arr = a.copy()
    x_limit, y_limit = arr.shape
    while (np.any(arr > MAX_PILE)):
        # spill sand to other adjacent cells
        for x in range(x_limit):
            for y in range(y_limit):
                if (arr[x, y] > MAX_PILE):
                    # amount of sand to send to each neighbour cell
                    sand = arr[x, y]//4
                    arr[x, y] = arr[x, y] % 4

                    # up
                    if (y + 1 < y_limit):
                        arr[x, y+1] += sand

                    # right
                    if (x + 1 < x_limit):
                        arr[x + 1, y] += sand

                    # down
                    if (y - 1 >= 0):
                        arr[x, y - 1] += sand

                    # left
                    if (x - 1 >= 0):
                        arr[x - 1, y] += sand
    return arr


def addSandToMiddle(arr, amount):
    x, y = arr.shape
    arr[x//2, y//2] = amount
