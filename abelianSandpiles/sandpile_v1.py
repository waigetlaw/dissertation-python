import numpy as np
import abelianHelper as helper
import drawSandpile as draw
from datetime import datetime
import math

startTime = datetime.now()

INITIAL_SAND = 50000
CHART_SIZE = math.floor(0.9 * math.sqrt(INITIAL_SAND))
array = np.zeros(shape=(CHART_SIZE, CHART_SIZE), dtype=np.int32)

helper.addSandToMiddle(array, INITIAL_SAND)
array = helper.stabilizev2(array)

draw.drawSandpile(array, "sandpile_" + str(INITIAL_SAND))

print(datetime.now() - startTime)

# 50000
# 0:03:34.659633

# 50000 v2 stabilize
# 0:02:54.248957

# 250000 v2
# 1:12:07.836735
