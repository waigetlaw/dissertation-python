import numpy as np
import abelianHelper as helper
import drawSandpile as draw
from datetime import datetime

# Identity = stab((cmax − stab(2 · cmax)) + cmax)

startTime = datetime.now()

n = 150

cmax = np.full(shape=(n, n), dtype=np.int32, fill_value=helper.MAX_PILE)

stab2cmax = helper.stabilizev2(2 * cmax)
print("calculated stab2cmax")
identity = helper.stabilizev2((cmax - stab2cmax) + cmax)


draw.drawSandpile(identity, "identity_" + str(n) + "x" + str(n))

print(datetime.now() - startTime)

# 50
# 0:00:07.370616

# 75
# 0:00:24.038488

# 100
# 0:01:02.780956

# 250
# 0:41:32.860889
