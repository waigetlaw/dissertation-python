from PIL import Image, ImageDraw
import os

# colors - list represents color for 0, 1, 2 and 3
COLOR_PALETTE = [(255, 255, 255), (115, 194, 251),
                 (0, 128, 255), (17, 30, 108)]


def drawSandpile(arr, filename):
    WIDTH, HEIGHT = arr.shape

    im = Image.new('RGB', (WIDTH, HEIGHT), COLOR_PALETTE[0])
    draw = ImageDraw.Draw(im)

    for x in range(WIDTH):
        for y in range(HEIGHT):
            draw.point([x, y], COLOR_PALETTE[arr[x, y]])

    outputDir = "abelianSandpiles/output/"

    if not os.path.exists(outputDir):
        os.makedirs(outputDir)

    im.save(os.path.join(outputDir, filename + ".png"), "png")
