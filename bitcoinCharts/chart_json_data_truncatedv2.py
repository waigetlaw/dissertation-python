from os import listdir
from os.path import isfile, join
import json
from matplotlib import pyplot as plt
import numpy as np
from functools import reduce

from datetime import datetime

startTime = datetime.now()

path = "../blockdata"
jsonFiles = [f for f in listdir(path) if (
    isfile(join(path, f)) and f.endswith('.json'))]

aggregatedData = []

for jsonFile in jsonFiles:
    print(f"Processing file: {jsonFile}")
    with open(join(path, jsonFile)) as f:
        aggregatedData += json.load(f)


def processData(result, blockData):
    date = datetime.utcfromtimestamp(
        blockData['mediantime']).strftime('%d/%m/%Y')
    totalFees = sum(map(lambda tx: tx['fees'], blockData['transactions']))
    if not date in result:
        result[date] = {'nTx': 0, 'totalFees': 0, 'blocks': 0}
    result[date] = {
        'nTx': result[date]['nTx'] + blockData['nTx'],
        'totalFees': result[date]['totalFees'] + totalFees,
        'blocks': result[date]['blocks'] + 1
    }
    return result


processedData = reduce(processData, aggregatedData, {})


# data for first and last dates will be incomplete
noOfTx = list(map(lambda data: data['nTx'], processedData.values()))[1:-1]
totalFees = list(
    map(lambda data: data['totalFees'], processedData.values()))[1:-1]
totalBlocks = list(
    map(lambda data: data['blocks'], processedData.values()))[1:-1]
dates = list(processedData.keys())[1:-1]
x_pos = np.arange(len(dates))

figure = plt.gcf()
figure.set_size_inches(11.75, 8.25)

ax1 = plt.subplot(311)
plt.bar(x_pos, noOfTx)

plt.title('No of Transactions per Day', fontsize=16)
plt.ylabel('No of Transactions')


ax2 = plt.subplot(312, sharex=ax1)
plt.bar(x_pos, totalFees)

plt.title('Total Fees per Day', fontsize=16)
plt.ylabel('Total Fees (Bitcoin)')

ax3 = plt.subplot(313, sharex=ax1)
plt.bar(x_pos, totalBlocks)

plt.title('Blocks per Day', fontsize=16)
plt.ylabel('Total Blocks')
plt.xticks(x_pos, dates, rotation=60)
plt.ylim(ymin=40)
plt.yticks(np.arange(0, 180, 20))
plt.xlabel('Date')

meanBlocks = sum(totalBlocks)/len(totalBlocks)
plt.axhline(meanBlocks, color='green', linewidth=2, linestyle='dashed')

plt.annotate(f"Mean Blocks: {int(meanBlocks)}", xy=(
    len(x_pos), meanBlocks), xytext=(-1, meanBlocks + 10))

plt.setp(ax1.get_xticklabels(), visible=False)
plt.setp(ax2.get_xticklabels(), visible=False)

plt.tight_layout()
plt.savefig('bitcoinCharts/bitcoinData_truncated.png', bbox_inches="tight")

print(datetime.now() - startTime)
