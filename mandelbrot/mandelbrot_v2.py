
from PIL import Image, ImageDraw
from datetime import datetime
import os

startTime = datetime.now()

MAX_ITERATIONS = 100

# Set tighter range
REAL_START = -2.1
REAL_END = 0.6
IMAGINARY_START = -1.2
IMAGINARY_END = 1.2

# calculate width to keep ratio
HEIGHT = 720
WIDTH = int(HEIGHT * (REAL_END - REAL_START) /
            (IMAGINARY_END - IMAGINARY_START))


def calcIterations(c: complex):
    """Calculates the number of iterations required for z to go above 2

    Parameters:
    (complex): complex number, C in the formula z = z^2 + C

    Returns:
    Number of iterations for abs(z) > 2 or -1 if bounded
    """

    z = 0
    for n in range(0, MAX_ITERATIONS):
        if (abs(z) > 2):
            return n
        z = z**2 + c
    return -1


im = Image.new('RGB', (WIDTH, HEIGHT), (0, 0, 0))
draw = ImageDraw.Draw(im)

for x in range(0, WIDTH):
    for y in range(0, HEIGHT):
        real = REAL_START + (x/WIDTH) * (REAL_END - REAL_START)
        imaginary = IMAGINARY_START + \
            (y/HEIGHT) * (IMAGINARY_END - IMAGINARY_START)
        c = complex(real, imaginary)

        iterations = calcIterations(c)

        color = 0 if iterations == -1 else 255 - \
            int(iterations * 255 / MAX_ITERATIONS)

        draw.point([x, y], (color, color, color))

outputDir = "mandelbrot/output/"

if not os.path.exists(outputDir):
    os.makedirs(outputDir)

im.save(os.path.join(outputDir, "mandelbrot_v2.pdf"), "pdf")

print(datetime.now() - startTime)

# 0:00:10.157870
