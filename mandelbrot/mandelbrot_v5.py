
from PIL import Image, ImageDraw
from datetime import datetime
import os

startTime = datetime.now()

MAX_ITERATIONS = 100

# Set tighter range
REAL_START = -2.1
REAL_END = 0.6
IMAGINARY_START = -1.2
IMAGINARY_END = 1.2

# v5 8K resolution
# calculate width to keep ratio
HEIGHT = 4320
WIDTH = int(HEIGHT * (REAL_END - REAL_START) /
            (IMAGINARY_END - IMAGINARY_START))


def calcIterations(c: complex):
    """Calculates the number of iterations required for z to go above 2

    Parameters:
    (complex): complex number, C in the formula z = z^2 + C

    Returns:
    Number of iterations for abs(z) > 2 or -1 if bounded
    """

    z = 0
    for n in range(0, MAX_ITERATIONS):
        if (abs(z) > 2):
            return n
        z = z**2 + c
    return -1


im = Image.new('HSV', (WIDTH, HEIGHT), (0, 0, 0))
draw = ImageDraw.Draw(im)

for x in range(0, WIDTH):
    for y in range(0, HEIGHT):
        real = REAL_START + (x/WIDTH) * (REAL_END - REAL_START)
        imaginary = IMAGINARY_START + \
            (y/HEIGHT) * (IMAGINARY_END - IMAGINARY_START)
        c = complex(real, imaginary)

        iterations = calcIterations(c)

        hue = int(255 * iterations / MAX_ITERATIONS)
        value = 0 if iterations == -1 else 255

        draw.point([x, y], (hue, 255, value))

outputDir = "mandelbrot/output/"

if not os.path.exists(outputDir):
    os.makedirs(outputDir)

im.convert("RGB").save(os.path.join(outputDir, "mandelbrot_v5_8K.pdf"), "pdf")

print(datetime.now() - startTime)

# 0:05:43.461863
