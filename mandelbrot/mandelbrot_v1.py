
from PIL import Image, ImageDraw
from datetime import datetime
import os


startTime = datetime.now()

MAX_ITERATIONS = 100

REAL_START = -2
REAL_END = 2
IMAGINARY_START = -2
IMAGINARY_END = 2

WIDTH = 1280
HEIGHT = 720


def calcIterations(c: complex):
    """Calculates the number of iterations required for z to go above 2

    Parameters:
    (complex): complex number, C in the formula z = z^2 + C

    Returns:
    Number of iterations for abs(z) > 2 or -1 if bounded
    """

    z = 0
    for n in range(0, MAX_ITERATIONS):
        if (abs(z) > 2):
            return n
        z = z**2 + c
    return -1


im = Image.new('RGB', (WIDTH, HEIGHT), (0, 0, 0))
draw = ImageDraw.Draw(im)

for x in range(0, WIDTH):
    for y in range(0, HEIGHT):
        real = REAL_START + (x/WIDTH) * (REAL_END - REAL_START)
        imaginary = IMAGINARY_START + \
            (y/HEIGHT) * (IMAGINARY_END - IMAGINARY_START)
        c = complex(real, imaginary)

        iterations = calcIterations(c)

        color = 255 if iterations == -1 else 255 - \
            int(iterations * 255 / MAX_ITERATIONS)

        draw.point([x, y], (color, color, color))

outputDir = "mandelbrot/output/"

if not os.path.exists(outputDir):
    os.makedirs(outputDir)

im.save(os.path.join(outputDir, "mandelbrot_v1.pdf"), "pdf")

print(datetime.now() - startTime)
