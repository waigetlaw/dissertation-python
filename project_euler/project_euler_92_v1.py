from datetime import datetime

startTime = datetime.now()

count = 0
for n in range(1, 10):
    num = n
    while (num != 1 and num != 89):
        square_total = 0
        for digit in str(num):
            square_total += (int(digit) ** 2)
        num = square_total
    if (num == 89):
        count += 1

print(count)

print(datetime.now() - startTime)

# 0:02:21.046662
