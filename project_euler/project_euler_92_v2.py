# Remove int -> str -> int casting

from datetime import datetime

startTime = datetime.now()

count = 0
for n in range(1, 1000000):
    num = n
    while (num != 1 and num != 89):
        square_sum = 0
        while (num > 0):
            square_sum += (num % 10) ** 2
            num = num // 10
        num = square_sum
    if (num == 89):
        count += 1

print(count)

print(datetime.now() - startTime)

# 0:01:46.738765
