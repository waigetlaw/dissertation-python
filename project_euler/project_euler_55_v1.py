from datetime import datetime

startTime = datetime.now()


def isPalindromicNumber(n):
    strDigits = [d for d in str(n)]
    strDigits.reverse()
    return ''.join(strDigits) == str(n)


def reverseNumber(n):
    strDigits = [d for d in str(n)]
    strDigits.reverse()
    return int(''.join(strDigits))


def isLychrel(n):
    n = n + reverseNumber(n)
    count = 0
    while not isPalindromicNumber(n):
        n = n + reverseNumber(n)
        count += 1
        if count > 50:
            return True
    return False


def countLychrel():
    total = 0
    for i in range(1, 10000):
        if (isLychrel(i)):
            total += 1
    return total


print(countLychrel())

print(datetime.now() - startTime)

# 0:00:00.131247
