# Remove int -> str -> int casting

from datetime import datetime
from functools import reduce

startTime = datetime.now()


def countEndsIn89(totalCount, currentNumber):
    num = currentNumber
    while (num != 1 and num != 89):
        square_sum = 0
        while (num > 0):
            square_sum += (num % 10) ** 2
            num = num // 10
        num = square_sum
    if (num == 89):
        return totalCount + 1
    return totalCount


print(reduce(countEndsIn89, range(1, 10000000), 0))

print(datetime.now() - startTime)

# 0:01:21.071748
