from datetime import datetime

startTime = datetime.now()


def getCombinationCount(total, arr):
    possibleCombinations = 0

    def reduce(total, arr, index=0):
        nonlocal possibleCombinations
        item = arr[index]
        index += 1
        if total == 0:

            possibleCombinations += 1
            return
        if index >= len(arr):
            if total % item == 0:
                possibleCombinations += 1
            return
        if item > total:
            return reduce(total, arr, index)

        while True:
            reduce(total, arr, index)
            total -= item
            if (total < 0):
                break

    reduce(total, arr)
    return possibleCombinations


print(getCombinationCount(500, [200, 100, 50, 20, 10, 5, 2, 1]))

print(datetime.now() - startTime)

# 0:00:00.030014
